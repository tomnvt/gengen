class TestInputs:

    some_cool_screen = {'screen_name': 'SomeCoolScreen', 'rows': [{
        'name': 'imgProduct',
        'view_type': 'UIImageView',
        'custom_type': '',
        'data_entry': 'imageUrl',
        'data_type': 'String',
        'interaction': 0,
        'reused': 0,
        'rows': [],
    }, {
        'name': 'btnAddToSaved',
        'view_type': 'UIButton',
        'custom_type': '',
        'data_entry': '',
        'data_type': '',
        'interaction': 1,
        'reused': 0,
        'rows': [],
    }, {
        'name': 'lblName',
        'view_type': 'UILabel',
        'custom_type': '',
        'data_entry': 'name',
        'data_type': 'String',
        'interaction': 0,
        'reused': 0,
        'rows': [],
    }, {
        'name': 'viewPrice',
        'view_type': 'Custom',
        'custom_type': 'PriceView',
        'data_entry': 'price',
        'data_type': 'Price',
        'interaction': 0,
        'reused': 0,
        'rows': [{
            'name': 'lblBasePrice',
            'view_type': 'UILabel',
            'custom_type': '',
            'data_entry': '',
            'data_type': '',
            'interaction': 0,
            'reused': 0,
            'rows': [],
        }, {
            'name': 'lblCurrentPrice',
            'view_type': 'UILabel',
            'custom_type': '',
            'data_entry': '',
            'data_type': '',
            'interaction': 0,
            'reused': 0,
            'rows': [],
        }],
    }]}

    common_networking = {
        'networking_type': 'CommonNetworking',
        'rows': [{
            'request_name': 'getIdentityServer',
            'base_url': '',
            'path': '/api/v1/system/identityServerEndpoints',
            'method': 'get',
            'rows': []
        }, {
            'request_name': 'getLoginStatus',
            'base_url': '',
            'path': '/api/v1/system/loginInfo/loginStatus',
            'method': 'get',
            'rows': []
        }, {
            'request_name': 'getDeviceAccessToken',
            'base_url': '',
            'path': '/connect/token',
            'method': 'post',
            'rows': []
        }, {
            'request_name': 'getUserAccessToken',
            'base_url': '',
            'path': '/connect/token',
            'method': 'post',
            'rows': []
        }]
    }

    api_response_model = {
        'model_name': 'SomeApiResponseModel',
        'rows': [{
            'property': 'firstProperty',
            'type': 'String',
            'rows': []
        }, {
            'property': 'secondProperty',
            'type': 'Int',
            'rows': []
        }, {
            'property': 'thirdProperty',
            'type': 'Submodel',
            'rows': []
        }]
    }

    product_model = {
        'entity_name': 'Product',
        'primary_key': 'code',
        'rows': [{
            'property_name': 'code',
            'property_type': 'String?',
            'type_in_db': '',
            'custom_type': '',
            'coding_key': '',
            'dynamic': 1,
            'list': 0,
            'rows': []
        }, {
            'property_name': 'imageURL',
            'property_type': 'String?',
            'type_in_db': '',
            'custom_type': '',
            'coding_key': '',
            'dynamic': 1,
            'list': 0,
            'rows': []
        }, {
            'property_name': 'colorName',
            'property_type': 'String?',
            'type_in_db': '',
            'custom_type': '',
            'coding_key': '',
            'dynamic': 1,
            'list': 0,
            'rows': []
        }, {
            'property_name': 'colorHexCode',
            'property_type': 'String?',
            'type_in_db': '',
            'custom_type': '',
            'coding_key': '',
            'dynamic': 1,
            'list': 0,
            'rows': []
        }, {
            'property_name': 'colorImageURL',
            'property_type': 'String?',
            'type_in_db': '',
            'custom_type': '',
            'coding_key': '',
            'dynamic': 1,
            'list': 0,
            'rows': []
        }, {
            'property_name': 'alternativeImageURLs',
            'property_type': 'String',
            'type_in_db': '',
            'custom_type': '',
            'coding_key': '',
            'dynamic': 0,
            'list': 1,
            'rows': []
        }, {
            'property_name': 'conceptCode',
            'property_type': 'String?',
            'type_in_db': '',
            'custom_type': '',
            'coding_key': '',
            'dynamic': 1,
            'list': 0,
            'rows': []
        }, {
            'property_name': 'saveDate',
            'property_type': 'Date',
            'type_in_db': '',
            'custom_type': '',
            'coding_key': '',
            'dynamic': 1,
            'list': 0,
            'rows': []
        }]
    }

    all_creatures = {
        'module_name': 'AllCreatures',
        'package': 'com.nvt.creaturemon.allcreatures',
        'layout_name': 'all_creatures',
        'model_type': 'CreaturePreview',
        'rows': [{
            'component_id': 'avatarListItem',
            'component_type': 'ImageView',
            'data_entry': 'imageURL',
            'data_type': 'String',
            'default_value': '""',
            'rows': []
        }, {
            'component_id': 'name',
            'component_type': 'TextView',
            'data_entry': 'name',
            'data_type': 'String',
            'default_value': '""',
            'rows': []
        }, {
            'component_id': 'hitPoints',
            'component_type': 'TextView',
            'data_entry': 'hitPoints',
            'data_type': 'Int',
            'default_value': '0',
            'rows': []
        }],
        'selected_template': 'MVVM_RecyclerView_UICollectionView'
    }