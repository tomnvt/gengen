from unittest import TestCase
from sources.modules.row_based_generator.helpers.config_helper import ConfigHelper
from sources.modules.row_based_generator.row_based_generator_config import RowBasedGeneratorConfig


class ConfigHelperTests(TestCase):

    config_helper = ConfigHelper

    """
    Just a simple test for ensuring that ConfigHelper is able to provide config.
    """
    def test_config_helper(self):
        config = self.config_helper.get_config()
        assert isinstance(config, RowBasedGeneratorConfig)
