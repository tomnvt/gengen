from unittest import TestCase
from sources.modules.row_based_generator.row_based_generator_config import RowBasedGeneratorConfig
from sources.modules.row_based_generator.ui.components.rows_section.row import Row
from tests.test_inputs import TestInputs


class RowTests(TestCase):

    class MockViewHandler:

        content = """{
            'header': {
                "Screen name": {
                    "id": "screen_name",
                    "type": 'TextField'
                }
            },
    
            'row': {
                "View name": {
                    "id": "name",
                    "type": "TextField"
                },
                "View type": {
                    "id": "view_type",
                    "type": "DropDownMenu",
                    "options": [
                        "UIView",
                        "UILabel",
                        "UIButton",
                        "UIImageView",
                        "UITableView",
                        "UICollectionView",
                        "UIScrollView",
                        "UIStackView",
                        "UITextField",
                        "UITextView",
                        "WKWebView",
                        "Custom"
                    ]
                },
                "Custom type": {
                    "id": "custom_type",
                    "type": "TextField"
                },
                "Data": {
                    "id": "data_entry",
                    "type": "TextField"
                },
                "Data type": {
                    "id": "data_type",
                    "type": "TextField"
                },
                "Interaction": {
                    "id": "interaction",
                    "type": "Checkbox"
                },
                "Reused": {
                    "id": "reused",
                    "type": "Checkbox"
                }
            },
    
            'output_folder_name_key': 'screen_name',
    
            'rules': [
                '#view_component# if row.reused and row.custom_type'
            ]
        }"""

        rows_frame = None
        config = RowBasedGeneratorConfig(content)

    """
    Test for creating a Row object from a dictionary and vice-versa
    """
    def test(self):
        input_dict = TestInputs.some_cool_screen['rows'][0]
        row_from_dict = Row.from_dictionary(input_dict, 1, self.MockViewHandler())
        assert row_from_dict.to_dictionary() == input_dict
