
func bindDataTo#name#() {
    viewModel.getModel()
        .map { $0.#data_entry# }
        .subscribe { [weak self] in
            // TODO: add value assignment
        }
        .disposed(by: disposeBag)
}