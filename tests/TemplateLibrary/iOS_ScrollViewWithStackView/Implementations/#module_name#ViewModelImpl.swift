import RxCocoa
import RxSwift

class #module_name#ViewModelImpl {

    // MARK: - Aliases
    typealias ScreenModel = #module_name#ScreenModel

    // MARK: - Properties
    private let model = BehaviorRelay<ScreenModel?>(value: nil)

    // MARK: - Init
    init() {}
}

extension #module_name#ViewModelImpl: #module_name#ViewModel {

    // MARK: - Data providing
    func getModel() -> Observable<ScreenModel> {
        return model.asObservable()
            .filter { $0 != nil }
            .map { $0! }
    }
}
