import Swinject

// swiftlint:disable identifier_name
// swiftlint:disable force_unwrapping
class #module_name#: Module {

    static func registerDependencies(container: Container) {
        container.register(#module_name#ViewController.self) { r in
            let viewModel = r.resolve(#module_name#ViewModel.self)!
            let interactionHandler = r.resolve(#module_name#InteractionHandler.self)!
            return #module_name#ViewController(viewModel: viewModel,
                                               interactionHandler: interactionHandler)
        }

        container.register(%module_nameViewModel.self) { r in
            #module_name#ViewModelImpl()
        }

        container.register(%module_nameInteractionHandler.self) { r in
            let viewModel = r.resolve(#module_name#ViewModel.self)!
            let router = r.resolve(#module_name#Router.self)!
            return #module_name#InteractionHandlerImpl(viewModel: viewModel,
                                                       router: router)
        }

        container.register(#module_name#Router.self) { r in
            let mainRouter = r.resolve(MainRouter.self)!
            return #module_name#RouterImpl(mainRouter: mainRouter)
        }
    }
}
