from unittest import TestCase

from sources.modules.row_based_generator.generation_engine.row_input_adjuster import RowInputAdjuster
from sources.modules.row_based_generator.model.user_input import UserInput
from sources.modules.row_based_generator.key_value_storage import *
from tests.test_inputs import TestInputs
from sources.utils import util_functions

expected_result = {'screen_name': 'SomeCoolScreen', 'rows': [{
    'name': 'btnAddToSaved',
    'view_type': 'UIButton',
    'custom_type': '',
    'data_entry': '',
    'data_type': '',
    'interaction': 1,
    'reused': 0,
    'rows': [],
}]}

expected_sub_template = """
extension #screen_name#ViewController {

    func function() {
        #interaction_binding.if(interaction)#
    }

     #interaction_binding_function.if(interaction)#
}
""".strip()


class SpecificationInterpreterTests(TestCase):

    interpreter = RowInputAdjuster()
    key_value_storage = KeyValueStorage()

    def test_user_input_adjustment(self):
        expected_user_input = UserInput(expected_result)
        placeholder = '#interaction_binding_extension.if(interaction)#'
        user_input = UserInput(TestInputs.some_cool_screen.copy())
        result = self.interpreter.adjust_user_input_for_placeholder(placeholder, user_input)
        assert result.data == expected_user_input.data

    def test_top_level_specification_interpretations(self):
        self.key_value_storage.set_value(KeyValueStorageKey.SELECTED_TEMPLATE_FOLDER,
                                         os.getcwd() + os.sep + 'Resources/TestTemplates')
        user_input = UserInput(TestInputs.some_cool_screen)
        template = util_functions.read_file(self.key_value_storage.selected_template_folder + os.sep + 'Unstructured' +
                                            os.sep +  '_TestTemplate12.txt')

        assert '#interaction_binding_extension.if_some_row_has(interaction)#' in template

        template = self.interpreter.interpret_top_level_specification(template, user_input)

        assert '#interaction_binding_extension.if_some_row_has(interaction)#' not in template
        assert expected_sub_template in template
