from unittest import TestCase
from sources.modules.row_based_generator.generation_engine.placeholder_list_handler import PlaceholderListHandler
from sources.modules.row_based_generator.model.user_input import UserInput
from tests.test_inputs import TestInputs


class PlaceholderListHandlerTests(TestCase):

    # TODO: separate test placeholders and expected outcomes
    test_placeholder = """
    stack.addArrangedSubviews(#name#,
                              ...)
    """.strip()

    expected_outcome = """
    stack.addArrangedSubviews(imgProduct,
                              btnAddToSaved,
                              lblName,
                              viewPrice)
    """.strip()

    test_placeholder2 = """
    lhs.#data_entry# == rhs.#data_entry#
    && ...
    """.strip()

    expected_outcome2 = """
    lhs.imageUrl == rhs.imageUrl
    && lhs.name == rhs.name
    && lhs.price == rhs.price
    """.strip()

    test_placeholder3 = """
    layout(#name#,
           0,
           ...)
    """.strip()

    expected_outcome3 = """
    layout(imgProduct,
           0,
           btnAddToSaved,
           0,
           lblName,
           0,
           viewPrice)
    """.strip()

    test_placeholder4 = """
    data class CreaturePreview(val #data_entry#: #data_type# = #default_value#,
                               ...)
    """.strip()

    expected_outcome4 = """
    data class CreaturePreview(val imageURL: String = "",
                               val name: String = "",
                               val hitPoints: Int = 0)
    """.strip()

    def test_placeholder_list_handler(self):
        handler = PlaceholderListHandler()
        user_input = UserInput(TestInputs.some_cool_screen).rows
        result = handler.handle_placeholder(self.test_placeholder, user_input)
        assert result == self.expected_outcome

    def test_placeholder_list_handler2(self):
        handler = PlaceholderListHandler()
        user_input = UserInput(TestInputs.some_cool_screen).rows

        result = handler.handle_placeholder(self.test_placeholder2, user_input)
        assert result == self.expected_outcome2

    def test_placeholder_list_handler3(self):
        handler = PlaceholderListHandler()
        user_input = UserInput(TestInputs.some_cool_screen).rows

        result = handler.handle_placeholder(self.test_placeholder3, user_input)
        assert result == self.expected_outcome3

    def test_placeholder_with_content_before_first_placeholder(self):
        handler = PlaceholderListHandler()
        user_input = UserInput(TestInputs.all_creatures).rows

        result = handler.handle_placeholder(self.test_placeholder4, user_input)
        assert result == self.expected_outcome4
