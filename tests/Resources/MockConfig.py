from sources.modules.row_based_generator.row_based_generator_config import RowBasedGeneratorConfig
import os

from sources.utils import util_functions


class MockConfig(RowBasedGeneratorConfig):

    def __init__(self):
        test_config = os.getcwd() + os.sep + 'Resources' + os.sep + 'test_config.conf'
        config = util_functions.read_file(test_config)
        super().__init__(config)
