import Moya

enum #networking_type#TargetType: TargetType {

    #request_case#
}

extension #networking_type#TargetType {

    var baseURL: URL {
        // swiftlint:disable force_unwrapping
        switch self {
        #base_url_case#
        }
    }

    var path: String {
        switch self {
        #path_case#
        }
    }

    var method: Moya.Method {
        switch self {
        #method_case#
        }
    }

    var sampleData: Data {
        switch self {
        default:
            return Data()
        }
    }

    var task: Task {
        switch self {
        #task_case#
        }
    }

    var headers: [String: String]? {
        switch self {
        #headers_case#
        }
    }
}
