struct #request_name#RequestSpec {

    var baseURL: String = "#base_url#"
    var path: String = "#path#"
    var method: Moya.Method = .#method#
    var task: Task = .requestPlain
    var headers: [String : String] = [:]

    // TODO: customize init if needed
    init() {}
}
