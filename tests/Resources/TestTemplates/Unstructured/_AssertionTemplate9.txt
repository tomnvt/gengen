9. There are some sub-placeholder with a sub-placeholder with the first ones specified with if

extension SomeCoolScreenViewController {

    func function() {
        bindBtnAddToSavedInteraction()
    }

     func bindbtnAddToSavedInteraction() {
         screenView.btnAddToSaved.tapGesture()
             .when(.recognized)
             .subscribe(onNext: { [weak self] _ in
                 // TODO: handle interactions
             })
             .disposed(by: disposeBag)
     }
}
