import copy
from unittest import TestCase
from sources.modules.row_based_generator.generation_engine.row_rules_handler import RowRulesHandler
from sources.modules.row_based_generator.key_value_storage import *
from sources.modules.row_based_generator.model.user_input import UserInput
from tests.Resources.MockConfig import MockConfig
from tests.test_inputs import TestInputs
from sources.utils import containers
from sources.utils.list_files import list_files

expected = """
import Stevia

class PriceView: UIView {

    // MARK: - Views
    lazy var scroll = UIScrollView()
    lazy var stack = UIStackView()
    lazy var content = UIView()
    lazy var lblBasePrice = UILabel()
    lazy var lblCurrentPrice = UILabel()

    // MARK: - Init
    init() {
        super.init(frame: .zero)

        setupViews()
        layoutBaseView()
        addContentViewSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup
    private func setupViews() {
        // TODO: add additional setup
    }

    private func layoutBaseView() {
      sv(scroll)
      scroll.sv(content)
      scroll.fillContainer()
      scroll.layout(0,
                    |content.width(100%)|,
                    0)
      content.sv(stack)
    }

    private func addContentViewSubviews() {
        stack.addArrangedSubviews(lblBasePrice,
                                  lblCurrentPrice)
    }
}
""".strip()


class RowRulesHandlerTests(TestCase):

    # TODO: make this test able to run separately, add docs
    def test(self):
        output_folder = KeyValueStorage().get_value(KeyValueStorageKey.SELECTED_OUTPUT_FOLDER)
        user_input = UserInput(copy.deepcopy(TestInputs.some_cool_screen))
        handler = containers.OBJECTS.provide(RowRulesHandler)
        handler.handle(user_input, MockConfig())
        path = os.path.join(output_folder, 'SomeCoolScreen', 'components')
        files = list_files(path)
        content = util_functions.read_file(files[0]).strip()
        assert content == expected
