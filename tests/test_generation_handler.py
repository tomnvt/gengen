import copy
import shutil
from unittest import TestCase
from sources.modules.row_based_generator.key_value_storage import *
from sources.modules.row_based_generator.model.user_input import UserInput
from tests.test_inputs import TestInputs
from sources.utils.containers import Modules
from sources.utils.list_files import list_files


class GenerationHandlerTests(TestCase):

    resources_folder = os.getcwd() + os.sep + 'Resources'

    def tearDown(self):
        resources_folder = os.getcwd() + os.sep + 'Resources'
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_CONFIG_FILE,
                                    resources_folder + os.sep + 'test_config.conf')

    def test_without_rows_only_templates(self):
        """
        Integration test for checking that GenerationHandler
        properly communications with TargetFileCreator
        """
        user_input = UserInput(copy.deepcopy(TestInputs.some_cool_screen))
        template_folder_path = self.resources_folder + os.sep + 'TestTemplates' \
                               + os.sep + 'Structured' + os.sep + user_input.screen_name
        output_folder_path = self.resources_folder + os.sep + 'Output'

        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_TEMPLATE_FOLDER, template_folder_path)
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_OUTPUT_FOLDER, output_folder_path)

        handler = Modules.generation_handler
        handler.handle(user_input)
        created_folder_path = output_folder_path + os.sep + user_input.screen_name
        target_end_files = [file.split('/')[-1] for file in list_files(created_folder_path)]
        assert 'SomeCoolScreenView.txt' in target_end_files
        assert 'SomeCoolScreenViewController.txt' in target_end_files
        shutil.rmtree(os.getcwd() + '/Resources/Output/SomeCoolScreen')

    def test_with_rows_only_templates(self):
        """
        Integration test for checking that GenerationHandler
        properly communications with TargetFileCreator,
        and that the TargetFilesCreator properly creates files
        regarding the 'rows-only' config values
        """
        user_input = UserInput(copy.deepcopy(TestInputs.common_networking))
        template_folder_path = self.resources_folder + os.sep + 'TestTemplates' \
                               + os.sep + 'Structured' + os.sep + user_input.networking_type
        output_folder_path = self.resources_folder + os.sep + 'Output'
        config_file_path = self.resources_folder + os.sep + 'test_config_networking.conf'

        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_TEMPLATE_FOLDER, template_folder_path)
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_OUTPUT_FOLDER, output_folder_path)
        KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_CONFIG_FILE, config_file_path)

        handler = Modules.generation_handler
        handler.handle(user_input)
        # TODO: add checking
        # assert 'SomeCoolScreenView.txt' in target_end_files
        # assert 'SomeCoolScreenViewController.txt' in target_end_files
        shutil.rmtree(os.getcwd() + '/Resources/Output/CommonNetworking')
