from unittest import TestCase

import pytest

from sources.modules.row_based_generator.key_value_storage import *


class KeyValueStorageTests(TestCase):

    test_key = KeyValueStorageKey.TEST_KEY
    test_value = 'TestValue'
    storage = KeyValueStorage()

    def test_value_setting(self):
        """
        tests setting a value in KeyValueStorage
        """
        self.storage.set_value(self.test_key, self.test_value)
        stored_value = self.storage.get_value(self.test_key)
        assert stored_value == self.test_value

    def test_key_deletion(self):
        """
        tests deleting a value in KeyValueStorage
        """
        self.storage.set_value(self.test_key, self.test_value)
        self.storage.delete_key(self.test_key)
        with pytest.raises(ValueForKeyNotFound):
            self.storage.get_value(self.test_key)
