import uuid
import json
import tkinter as tk
from tkinter import ttk


class TreeViewVisualizer:

    @staticmethod
    def __j_tree(tree, parent, dic):
        for key in sorted(dic.keys()):
            TreeViewVisualizer.__process_dict_key(dic, key, parent, tree)

    @staticmethod
    def __process_dict_key(dic, key, parent, tree):
        uid = uuid.uuid4()
        if isinstance(dic[key], dict):
            tree.insert(parent, 'end', uid, text=key)
            TreeViewVisualizer.__j_tree(tree, uid, dic[key])
        elif isinstance(dic[key], tuple):
            tree.insert(parent, 'end', uid, text=str(key) + '()')
            TreeViewVisualizer.__j_tree(tree, uid, dict(enumerate(dic[key])))
        elif isinstance(dic[key], list):
            tree.insert(parent, 'end', uid, text=str(key) + '[]')
            TreeViewVisualizer.__j_tree(tree, uid, dict(enumerate(dic[key])))
        else:
            value = dic[key]
            if isinstance(value, str):
                value = value.replace(' ', '_')
            tree.insert(parent, 'end', uid, text=key, value=value if value is not None else 'None')

    @staticmethod
    def show_tree_view(input_rows):
        data = {}
        for row in input_rows:
            dictionary = row.to_dictionary()
            first_key = list(dictionary.keys())[0]
            data[dictionary[first_key]] = dictionary

        data = json.dumps(data)
        data = json.loads(data)
        root = tk.Tk()
        root.title("Tree View")
        root.columnconfigure(0, weight=1)
        root.rowconfigure(0, weight=1)

        tree_frame = ttk.Frame(root, padding="3")
        tree_frame.grid(row=0, column=0, sticky=tk.NSEW)

        tree = ttk.Treeview(tree_frame, columns='Values')
        tree.column('Values', width=100, anchor='center')
        tree.heading('Values', text='Values')
        TreeViewVisualizer.__j_tree(tree, '', data)
        tree.pack(fill=tk.BOTH, expand=1)

        root.update_idletasks()
        root.minsize(root.winfo_reqwidth(), root.winfo_reqheight())
        root.mainloop()
