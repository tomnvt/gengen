class ViewState:

    """ Wrapper for Tk widget state values. """
    enabled = 'normal'
    disabled = 'disabled'
