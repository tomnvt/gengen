from sources.modules.row_based_generator.ui.components.sub_components.button import ButtonComponent as Button
from sources.modules.row_based_generator.ui.components.sub_components.drop_down_menu import DropDownMenuComponent as DropDownMenu


class MainMenu:

    """" Properties """
    right_alignment_column_offset = 1

    """ Init """
    def __init__(self, superview, config):
        self.config = config
        self.first_row_views = []
        self.second_row_views = []

        self.first_row_views += [
            Button(superview.main_menu_frame, label="OUTPUT DIR", action=superview.on_output_dir_button_click),
            Button(superview.main_menu_frame, label="TEMPLATE LIBRARY", action=superview.on_templates_button_click),
            Button(superview.main_menu_frame, label="LOAD INPUT", action=superview.on_load_button_click),
            Button(superview.main_menu_frame, label="GENERATE", action=superview.on_generate_button_click)
        ]

        self.second_row_views += [Button(superview.main_menu_frame, "SHOW OUTPUT",
                                         action=superview.on_output_button_click),
                                  DropDownMenu(superview.main_menu_frame, 'templates',
                                               *self.config.__dict__['templates']),
                                  Button(superview.main_menu_frame, label="SAVE INPUT",
                                         action=superview.on_save_button_click),
                                  Button(superview.main_menu_frame, label="TREE VIEW",
                                         action=superview.on_visualize_button_click)]

        self.selected_templates = [component for component in self.second_row_views
                                   if component.get_id() == 'templates'][0]
        self.selected_templates.bind_to(superview.on_template_change)
        self.selected_templates.set_value(self.config.__dict__['selected_template'])

    def show(self):
        for index in range(len(self.first_row_views)):
            self.first_row_views[index].show(column=index, row=1)

        for index in range(len(self.second_row_views)):
            self.second_row_views[index].show(column=index, row=2)

    def hide(self):
        for view in self.first_row_views:
            view.hide()

        for view in self.second_row_views:
            view.hide()
