from tkinter import Entry
from sources.modules.row_based_generator.ui.components.sub_components import component


class TextFieldComponent(component.Component):

    def __init__(self, window, id, state='normal'):
        self.component_type = Entry(master=window, width=15)
        self.id = id
        self.component_type.config(disabledbackground="lightGray", state='disabled')
        self.component_type.config(stat=state)

    def get_value(self):
        return self.component_type.get()

    def set_value(self, text):
        current_text_length = len(self.get_value())
        self.component_type.delete(0, current_text_length)
        self.component_type.insert(0, text if text is not None else '')

    def set_state(self, state):
        self.component_type.config(state=state)
