from tkinter import Button
from sources.modules.row_based_generator.ui.components.sub_components.component import Component


class ButtonComponent(Component):

    def __init__(self, window, label, action):
        self.id = label
        width = int(len(label) * 1.5)
        self.component_type = Button(master=window, text=label,
                                     command=lambda: action(),
                                     width=width, height=2)

    def set_text(self, text):
        self.component_type.configure(text=text)
