from tkinter import Checkbutton, IntVar
from sources.modules.row_based_generator.ui.components.sub_components.component import Component


class CheckboxComponent(Component):

    def __init__(self, window, id):
        self.id = id
        self.current_value = IntVar()

        self.component_type = Checkbutton(window, variable=self.current_value)

    def set_value(self, value):
        self.current_value.set(1 if value else 0)

    def get_value(self):
        return self.current_value.get()
