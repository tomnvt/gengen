from ..sub_components.button import ButtonComponent as Button
from ..sub_components.label import LabelComponent as Label


class RowsSectionHeader:

    """ Init """
    def __init__(self, window, interaction_handler, config):
        names = [component_name for component_name in config.row]
        labels = [Label(window, name) for name in names]

        self.subviews = [Label(window, "Row index")] + labels
        self.subviews += [Button(window, "Add row", interaction_handler.on_add_subview_button_click)]

        self.back_button = Button(window, "Back", interaction_handler.on_back_button_click)

    """ Public """
    def show(self):
        for index in range(0, len(self.subviews)):
            self.subviews[index].show(column=index, row=3)

    def hide(self):
        for index in range(0, len(self.subviews)):
            self.subviews[index].hide()

    def show_back_button(self):
        column = len(self.subviews)
        self.back_button.show(column=column, row=3)

    def hide_back_button(self):
        self.back_button.hide()
