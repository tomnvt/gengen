from sources.utils.keys import Keys
from sources.modules.row_based_generator.ui.support_files.labels import Labels
from sources.modules.row_based_generator.ui.components.components_factory import ComponentsFactory
from sources.modules.row_based_generator.ui.components.sub_components.label import LabelComponent as Label
from sources.modules.row_based_generator.ui.components.sub_components.button import ButtonComponent as Button


class Row:

    """ Properties """
    top_offset = 4  # should be equal to main header rows count + 1

    """ Init """
    def __init__(self, row: int, view_handler):
        self.config = view_handler.config
        window = view_handler.rows_frame
        factory = ComponentsFactory(self.config)
        self.components = [Label(window, str(row))]
        self.components += factory.create_view_row_components(window)
        self.components += [Button(window, Labels.Rows,
                                   lambda: view_handler.on_show_subviews_button_click(self)),
                            Button(window, Labels.Remove,
                                   lambda: view_handler.on_remove_button_click(self))]

        self.rows = []
        self.grid_row = row + self.top_offset

        self.show()

    @staticmethod
    def from_dictionary(view_data, row, view_handler):
        sub_rows = []
        if len(view_data[Keys.rows]) != 0:
            for index in range(0, len(view_data[Keys.rows])):

                subview_row = Row.from_dictionary(view_data[Keys.rows][index], index, view_handler)
                sub_rows.append(subview_row)
        view_row = Row(row, view_handler)

        ids = []
        for key in view_handler.config.row:
            ids.append(view_handler.config.row[key][Keys.id])

        for id in ids:
            component = [component for component in view_row.components if component.id == id][0]
            try:
                value = view_data[id]
                component.set_value(value)
            except KeyError:
                continue

        view_row.rows = sub_rows
        view_row.hide()
        return view_row

    def to_dictionary(self):
        views = []
        for view in self.rows:
            views.append(view.to_dictionary())
        user_input_components = [component for component in self.components
                                 if type(component) is not Label and type(component) is not Button]
        dictionary = {}
        for component in user_input_components:
            dictionary[component.get_id()] = component.get_value()

        dictionary[Keys.rows] = views

        return dictionary

    """ Public """
    def show(self):
        column_index = 0
        for view in self.components:
            view.show(column=column_index, row=self.grid_row)
            column_index += 1

    def hide(self):
        column_index = 0
        for view in self.components:
            view.hide()
            column_index += 1

    def add_row(self, row):
        self.rows.append(row)

    def update_row_index(self, new_index):
        self.grid_row = new_index + self.top_offset
        self.components[0].set_text(str(new_index))
