from sources.modules.row_based_generator.ui.components.sub_components.label import LabelComponent as Label
from sources.modules.row_based_generator.ui.components.components_factory import ComponentsFactory


class Header:

    """" Properties """
    right_alignment_column_offset = 1

    """ Init """
    def __init__(self, superview, config):
        self.config = config
        self.first_row_views = []
        self.second_row_views = []
        factory = ComponentsFactory(config)

        labels = list(config.header.keys())
        self.first_row_views = [Label(superview, label=text) for text in labels]

        self.second_row_views = factory.create_main_header_components(superview)

    """ Public """
    def show(self):
        for index in range(0, len(self.first_row_views)):
            column = index if index < len(self.config.header.keys()) \
                else index + self.right_alignment_column_offset
            self.first_row_views[index].show(column=column, row=1)

        for index in range(0, len(self.second_row_views)):
            column = index if index < len(self.config.header.keys()) \
                else index + self.right_alignment_column_offset
            self.second_row_views[index].show(column=column, row=2)

    def hide(self):
        for view in self.first_row_views:
            view.hide()

        for view in self.second_row_views:
            view.hide()

    def set_input(self, user_input):
        for key in self.config.header:
            id = self.config.header[key]['id']
            component = [component for component in self.second_row_views if component.get_id() == id][0]
            try:
                component.set_value(user_input[id])
            except KeyError:
                pass

    def get_input(self):
        dictionary = {}
        for header_input in self.config.header:
            component = [component for component in self.second_row_views
                         if component.get_id() == self.config.header[header_input]['id']][0]
            dictionary[component.get_id()] = component.get_value()
        return dictionary
