from sources.modules.row_based_generator.model.user_input import UserInput
from sources.modules.row_based_generator.ui.main_view_handler import UIEvent


class RowBasedGeneratorModule:

    def __init__(self, main_view_handler, generation_handler):
        self.main_view_handler = main_view_handler
        self.generation_handler = generation_handler

    def start_ui(self):
        self.main_view_handler.event_subject.subscribe(self.process_ui_event)
        self.main_view_handler.run()

    def start_generator(self, inputs):
        user_input = UserInput(inputs)
        self.generation_handler.handle(user_input)

    def process_ui_event(self, data):
        if data[0] == UIEvent.GENERATE_BUTTON_TAPPED:
            self.start_generator(data[1])
