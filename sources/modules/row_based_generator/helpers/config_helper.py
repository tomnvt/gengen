import os

from sources.modules.row_based_generator.key_value_storage \
    import KeyValueStorage, KeyValueStorageKey, ValueForKeyNotFound
from sources.modules.row_based_generator.row_based_generator_config import RowBasedGeneratorConfig
from sources.utils import util_functions


class ConfigHelper:

    @staticmethod
    def get_config():
        working_directory = util_functions.get_working_directory()

        template_library_path = working_directory + os.sep + 'TemplateLibrary'
        dirs = os.listdir(template_library_path)
        dirs = [dir for dir in dirs if dir != '.DS_Store']
        try:
            config_file_path = KeyValueStorage().get_value(KeyValueStorageKey.SELECTED_CONFIG_FILE)
        except ValueForKeyNotFound:
            return RowBasedGeneratorConfig(templates=dirs)

        try:
            config_file_content = util_functions.read_file(config_file_path)
            return RowBasedGeneratorConfig(config_file_content, dirs)
        except FileNotFoundError:
            return RowBasedGeneratorConfig(templates=dirs)
        except IsADirectoryError:
            return RowBasedGeneratorConfig(templates=dirs)
