""" GenerationHandler """
import os
from sources.utils import util_functions
from sources.modules.row_based_generator.helpers.config_helper import ConfigHelper
from sources.modules.row_based_generator.key_value_storage import KeyValueStorage, KeyValueStorageKey, \
    ValueForKeyNotFound
from sources.utils.list_files import list_files


class GenerationHandler:

    config = None

    def __init__(self, target_files_creator, placeholder_replacer, row_rules_handler, rows_only_handler):
        """
        Lead object that handles code generation with provided user input
        :param target_files_creator: copies content of selected template folder and uses user input to modify file names
        accordingly
        :param placeholder_replacer: iterate over all files in target output folder and replaces all placeholders
        contained in those files
        :param row_rules_handler: object for handling 'row_rules' configuration
        :param rows_only_handler: object for handling 'rows_only' configuration
        """
        self.target_files_creator = target_files_creator
        self.placeholder_replacer = placeholder_replacer
        self.row_rules_handler = row_rules_handler
        self.rows_only_handler = rows_only_handler

    def handle(self, user_input):
        """
        User input handling is done in two phases:
        1a) files for templates that take the whole user input are created
        1b) the files are processed, meaning the placeholder replacer walks through the files
            and replaces all placeholders with appropriate values from user input
        2a) file for templates that take only individual rows as input are created
        2b) the files are processed similarly as in 1b, with adjusted user input so it contains only rows
        """
        self.config = ConfigHelper.get_config()

        try:
            output_folder = KeyValueStorage().get_value(KeyValueStorageKey.SELECTED_OUTPUT_FOLDER)
        except ValueForKeyNotFound:
            output_folder = os.getcwd() + os.sep + 'Output'
            KeyValueStorage().set_value(KeyValueStorageKey.SELECTED_OUTPUT_FOLDER, output_folder)

        self.target_files_creator.create_target_files_for_entire_user_input(user_input, self.config)

        output_folder = output_folder + os.sep + user_input.data[self.config.output_folder_name_key]
        files = list_files(output_folder)
        for file in files:
            if '.DS_Store' in file:
                continue

            file_content = util_functions.read_file(file)

            result = self.placeholder_replacer.process(file_content, user_input)
            util_functions.write_to_file(file, result)

        self.row_rules_handler.handle(user_input, self.config)
        self.target_files_creator.create_target_files_for_rows_only_input(user_input, self.config)
        self.rows_only_handler.handle(user_input)
