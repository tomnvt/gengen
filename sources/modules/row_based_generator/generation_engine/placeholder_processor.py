import re


class PlaceholderProcessor:

    @staticmethod
    def get_all_placeholders(view_template):
        return [placeholder for placeholder in re.findall(r'#\w*[._,()\w]*#', view_template)]
