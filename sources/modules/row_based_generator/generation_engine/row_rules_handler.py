import copy
import os
from sources.utils import util_functions
from sources.modules.row_based_generator.key_value_storage \
    import KeyValueStorage, KeyValueStorageKey
from sources.modules.row_based_generator.model.user_input import UserInput
from .placeholder_processor import PlaceholderProcessor


class RowRulesHandler(PlaceholderProcessor):

    def __init__(self, placeholder_replacer):
        self.placeholder_replacer = placeholder_replacer

    def handle(self, user_input, config):
        data_copy = copy.deepcopy(user_input.data)
        user_input_copy = UserInput(data_copy)
        template_folder = KeyValueStorage().get_value(KeyValueStorageKey.SELECTED_TEMPLATE_FOLDER)
        output_path = KeyValueStorage().get_value(KeyValueStorageKey.SELECTED_OUTPUT_FOLDER)
        target_folder = output_path + os.sep + user_input.data[config.output_folder_name_key]
        for rule in config.row_rules:
            self.__process_rule(rule, target_folder, template_folder, user_input_copy)

    def __process_rule(self, rule, target_folder, template_folder, user_input_copy):
        for row in user_input_copy.rows:
            if not self.__should_process_row(row, rule):
                continue

            row_copy = copy.deepcopy(row)
            parent_values = self.__replace_values_in_row(row_copy, rule)
            self.__delete_redundant_keys(parent_values, row_copy)
            self.__write_changes(row_copy, rule, target_folder, template_folder)

    @classmethod
    def __should_process_row(cls, row, rule):
        row_should_be_processed = True
        conditions = rule['condition'].split('+')
        for condition in conditions:
            if not row_should_be_processed:
                break
            if '!' in condition:
                row_should_be_processed = row[condition[1:]] == 0 or row[condition[1:]] == ''
            else:
                row_should_be_processed = row[condition] == 1 or row[condition] != ''
        return row_should_be_processed

    @classmethod
    def __replace_values_in_row(cls, row, rule):
        parent_values = rule['parent_values']
        for key in parent_values:
            row[parent_values[key]] = row[key]
            # del row[key]
        return parent_values

    @classmethod
    def __delete_redundant_keys(cls, parent_values, row):
        redundant_keys = [key for key in list(row.keys())
                          if key not in list(parent_values.values()) and key != 'rows']
        for key in redundant_keys:
            del row[key]

    def __write_changes(self, row, rule, target_folder, template_folder):
        sub_template_file = template_folder + os.sep + 'SubTemplates' + os.sep + rule['sub_template'] + '.txt'
        template = util_functions.read_file(sub_template_file)
        row_user_input_copy = UserInput(row.copy())
        template = self.placeholder_replacer.process(template, row_user_input_copy)
        try:
            util_functions.write_to_file(target_folder + os.sep + rule['output_folder']
                                         + rule['output_name_prefix']
                                         + row[rule['output_name_key']]
                                         + rule['output_name_suffix'],
                                         template)
        except FileNotFoundError:
            os.makedirs(target_folder + os.sep + rule['output_folder'])
            util_functions.write_to_file(target_folder + os.sep + rule['output_folder']
                                         + rule['output_name_prefix']
                                         + row[rule['output_name_key']]
                                         + rule['output_name_suffix'],
                                         template)
