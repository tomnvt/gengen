import os
from sources.modules.row_based_generator.model.user_input import UserInput
from sources.modules.row_based_generator.helpers.config_helper import ConfigHelper
from sources.modules.row_based_generator.row_based_generator_config import KeyValueStorage, \
    KeyValueStorageKey
from sources.modules.row_based_generator.generation_engine.placeholder_processor import PlaceholderProcessor
from sources.utils.list_files import list_files


class RowsOnlyHandler(PlaceholderProcessor):

    def __init__(self, placeholder_replacer):
        self.placeholder_replacer = placeholder_replacer

    def handle(self, user_input):
        config = ConfigHelper.get_config()
        for entry in config.rows_only:
            placeholder = self.get_all_placeholders(entry)[0]
            entry_without_placeholder = entry.replace(placeholder, '')
            output_dir_name = user_input.data[config.output_folder_name_key]
            output_directory = KeyValueStorage().get_value(KeyValueStorageKey.SELECTED_OUTPUT_FOLDER)
            listed = list_files(output_directory + os.sep + output_dir_name)
            target_files = [file for file in listed if entry_without_placeholder in file]
            target_files = sorted(target_files)

            user_input.data['rows'] = sorted(user_input.rows, key=lambda i: i['request_name'])

            for target_file_path, index in zip(target_files, range(len(target_files))):
                file = open(target_file_path, 'r')
                content = file.read()
                file.close()
                updated_user_input = UserInput(user_input.data['rows'][index])

                result = self.placeholder_replacer.process(content, updated_user_input)
                file = open(target_file_path, 'w')
                file.write(result)
                file.close()
