import re


class PlaceholderInterpreter:

    @staticmethod
    def adjust_placeholder(placeholder, row):
        placeholder_components = PlaceholderInterpreter.parse_placeholder(placeholder)

        if placeholder_components['specifier'] == 'switch_if_provided':
            return '#' + placeholder_components['argument'] + '#' \
                if row[placeholder_components['argument']] \
                else '#' + PlaceholderInterpreter.get_original_placeholder(placeholder) + '#'

        if placeholder_components['specifier'] == 'if' or placeholder_components['specifier'] == 'if_has':
            condition_satisfied = False
            original_argument = '#' + PlaceholderInterpreter.get_original_placeholder(placeholder) + '#'
            if isinstance(row[placeholder_components['argument']], int):
                condition_satisfied = row[placeholder_components['argument']] == 1
            elif isinstance(row[placeholder_components['argument']], str):
                condition_satisfied = row[placeholder_components['argument']] != ''
            return original_argument if condition_satisfied else None

        if placeholder_components['specifier'] == 'if_not' or placeholder_components['specifier'] == 'if_has_not':
            condition_satisfied = False
            original_argument = '#' + PlaceholderInterpreter.get_original_placeholder(placeholder) + '#'
            if isinstance(row[placeholder_components['argument']], int):
                condition_satisfied = row[placeholder_components['argument']] == 0
            elif isinstance(row[placeholder_components['argument']], str):
                condition_satisfied = row[placeholder_components['argument']] == ''
            return original_argument if condition_satisfied else None

        return placeholder

    @staticmethod
    def parse_placeholder(placeholder_input):
        placeholder = PlaceholderInterpreter.get_placeholder(placeholder_input)
        specifier = PlaceholderInterpreter.get_specifier(placeholder_input)
        argument = PlaceholderInterpreter.get_argument(placeholder_input)
        return {
            'placeholder': placeholder,
            'specifier': specifier,
            'argument': argument
        }

    @staticmethod
    def get_placeholder(placeholder):
        return re.findall(r'\w*[.]', placeholder)[0][:-1]

    @staticmethod
    def get_specifier(placeholder):
        if '(' in placeholder:
            return re.findall('\\..*[(]', placeholder)[0][1:-1]
        else:
            return re.findall('\\..*#', placeholder)[0][1:-1]

    @staticmethod
    def get_argument(placeholder):
        if '(' in placeholder:
            return re.findall(r'[(].*[)]', placeholder)[0][1:-1]
        else:
            return ''

    @staticmethod
    def get_original_placeholder(placeholder):
        return re.findall(r'#\w*[.]', placeholder)[0][1:-1]