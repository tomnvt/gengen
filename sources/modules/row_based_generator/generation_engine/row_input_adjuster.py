from .placeholder_interpreter import PlaceholderInterpreter
from .placeholder_processor import PlaceholderProcessor
from sources.modules.row_based_generator.model.user_input import UserInput
from sources.modules.row_based_generator.key_value_storage import *
from sources.utils import util_functions


class RowInputAdjuster(PlaceholderProcessor):

    def adjust_user_input_for_placeholder(self, placeholder, user_input):
        user_input_copy = UserInput(user_input.data.copy())
        placeholder_components = PlaceholderInterpreter.parse_placeholder(placeholder)
        # TODO: this is semi-duplicate with processing of 'if' specifier in PlaceholderInterpreter
        if placeholder_components['specifier'] == 'if_has' or placeholder_components['specifier'] == 'if':
            self.__handle_if_has(placeholder_components, user_input_copy)
        elif placeholder_components['specifier'] == 'if_has_not' or placeholder_components['specifier'] == 'if_not':
            self.__handle_if_has_not(placeholder_components, user_input_copy)
        elif placeholder_components['specifier'] == 'for_rows':
            return user_input_copy
        return user_input_copy

    @classmethod
    def __handle_if_has(cls, placeholder_components, user_input_copy):
        filtered_rows = [row for row in user_input_copy.rows
                         if row[placeholder_components['argument']] == 1
                         or row[placeholder_components['argument']]]
        user_input_copy.rows = filtered_rows
        user_input_copy.data['rows'] = filtered_rows

    @classmethod
    def __handle_if_has_not(cls, placeholder_components, user_input_copy):
        filtered_rows = [row for row in user_input_copy.rows
                         if row[placeholder_components['argument']] == 0
                         or row[placeholder_components['argument']] == '']
        user_input_copy.rows = filtered_rows
        user_input_copy.data['rows'] = filtered_rows

    def interpret_top_level_specification(self, template, user_input):
        sub_templates_folder = KeyValueStorage().get_value(KeyValueStorageKey.SELECTED_TEMPLATE_FOLDER) \
                               + os.sep + 'SubTemplates'
        placeholders = self.get_all_placeholders(template)

        for placeholder in placeholders:
            if '.' not in placeholder:
                continue

            placeholder_components = PlaceholderInterpreter.parse_placeholder(placeholder)
            if placeholder_components['specifier'] == 'if_some_row_has':
                values = [row[placeholder_components['argument']] for row in user_input.rows]

                if 1 in values or [value for value in values if value] != []:
                    file_path = sub_templates_folder + os.sep + placeholder_components['placeholder'] + '.txt'
                    sub_template = util_functions.read_file(file_path)
                    template = template.replace(placeholder, sub_template.rstrip())
                else:
                    return template.replace(placeholder, '')
            if placeholder_components['specifier'] == 'for_rows':
                return template
        return template
