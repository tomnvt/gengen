import os


def list_files(start_path):
    all_files = []
    for current_path, folders, files in os.walk(start_path):
        [all_files.append(current_path + os.sep + file) for file in files]
    if '.DS_STORE' in all_files:
        all_files.remove('.DS_STORE')
    return all_files
