//
//  #model_type#.swift
//
//  Created by GenGen.
//

struct #model_type# {

    #properties.if(data_entry)#
}

extension #model_type#: Equatable {

    static func == (lhs: #model_type#, rhs: #model_type#) -> Bool {
        return #equatable.if(data_entry)#
    }
}