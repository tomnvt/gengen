//
//  #module_name#Cell.swift
//
//  Created by GenGen.
//

import Stevia

class #module_name#Cell: UICollectionViewCell {

    // MARK: - Aliases
    typealias Delegate = #module_name#CellDelegate
    typealias Model = #model_type#

    // MARK: - Views
    #views#

    // MARK: - Properties
    private var delegate: Delegate?

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup
    private func setupView() {
        // TODO: add setup
    }

    private func layoutViews() {
        #cell_layout#
    }
}

extension #module_name#Cell {

    func setup(model: Model) {
        // TODO: set model values to value properties
        #cell_setup.if_has(data_entry)#
    }

    func setDelegate(_ delegate: Delegate?) {
        self.delegate = delegate
    }
}
