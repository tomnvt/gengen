//
//  #module_name#CollectionView.swift
//
//  Created by GenGen.
//

import RxDataSources
import UIKit

class #module_name#CollectionView: UICollectionView {

    // MARK: - Aliases
    typealias Cell = #module_name#Cell
    typealias CellModel = Cell.Model
    typealias CellDelegate = #module_name#CellDelegate
    typealias Layout = UICollectionViewFlowLayout
    typealias RxDataSourceAnimated = RxCollectionViewSectionedAnimatedDataSource

    private let cellTypeString = String(describing: Cell.self)

    // MARK: - Init
    init(layout: Layout) {
        super.init(frame: .zero, collectionViewLayout: layout)

        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup
    private func setupView() {
        register(Cell.self, forCellWithReuseIdentifier: cellTypeString)
    }
}

extension #module_name#CollectionView {

    func getDataSource(delegate: CellDelegate? = nil) -> RxDataSourceAnimated<#model_type#Section> {
    return RxDataSourceAnimated<#model_type#Section>(configureCell: { [weak self] _, collectionView, indexPath, model in
            self?.getCellConfiguration(delegate: delegate,
                                       collectionView: collectionView,
                                       indexPath: indexPath,
                                       model: model) ?? UICollectionViewCell()
        })
    }

    private func getCellConfiguration(delegate: CellDelegate?,
                                      collectionView: UICollectionView,
                                      indexPath: IndexPath,
                                      model: CellModel) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellTypeString,
                                                      for: indexPath) as? Cell ?? Cell()
        cell.setup(model: model)
        cell.setDelegate(delegate)
        return cell
    }
}

protocol #module_name#CellDelegate : class {}
