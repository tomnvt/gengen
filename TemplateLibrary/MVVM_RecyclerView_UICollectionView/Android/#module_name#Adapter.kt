package #package#.#module_name.lowercased#

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import #package#.R
import #package#.app.inflate
import kotlinx.android.synthetic.main.#layout_name#_activity_list_item.view.*

class #module_name#Adapter(private val data: MutableList<#model_type#>)
  : RecyclerView.Adapter<#module_name#Adapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.#layout_name#_activity))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount() = data.size

    fun update(model: List<#model_type#>) {
        this.data.clear()
        this.data.addAll(model)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private lateinit var data: #model_type#

        init { itemView.setOnClickListener(this) }

        fun bind(data: #model_type#) {
            this.data = data
            // TODO: adjust data values assignment
            #item_view_properties#
        }

        override fun onClick(p0: View?) {
            // TODO: handle onClick
        }
    }
}