//
//  #entity_name#DBEntity.swift
//
//  Created by GenGen.
//

import RealmSwift

class #entity_name#DBEntity: Object {

    // MARK: - Properties
    #property_addition#

    #primary_key_addition#

    // MARK: - Init
    convenience init(from entity: #entity_name#Entity) {
        self.init()
        #property_initialization.if_has_not(list)#
        #list_property_initialization.if_has(list)#
    }
}

extension #entity_name#DBEntity: #entity_name#Entity {

    #list_property_conformance.if_has(list)#

    #custom_entity_conformance.if_has(custom_type)#
}
