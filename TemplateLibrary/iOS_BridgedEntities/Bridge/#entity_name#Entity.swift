//
//  #entity_name#Model.swift
//
//  Created by GenGen.
//

protocol #entity_name#Entity {

    #protocol_property.if_has_not(custom_type)#
    #custom_protocol_property.if_has(custom_type)#
}
