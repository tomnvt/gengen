//
//  #entity_name#APIEntity.swift
//
//  Created by GenGen.
//

struct #entity_name#APIEntity: Codable {

    #api_property_addition#

    enum CodingKeys: String, CodingKey {
        #coding_key#
    }
}
