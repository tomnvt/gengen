//
//  #networking_type#Impl.swift
//
//  Created by GenGen.
//

import Moya
import RxSwift

class #networking_type#Impl {

    // MARK: - Aliases
    typealias TargetType = #networking_type#TargetType

    // MARK: - Properties
    private let provider: MoyaProvider<TargetType> = {
        return MoyaProvider(endpointClosure: MoyaProvider.defaultEndpointMapping,
                            stubClosure: MoyaProvider.neverStub,
                            manager: MoyaProvider<TargetType>.defaultAlamofireManager(),
                            plugins: [NetworkLoggerPlugin(cURL: true)],
                            trackInflights: false)
    }()

    private let baseURL = "#base_url#"

    // MARK: - Init
    init() {}
}

extension #networking_type#Impl: #networking_type# {

    #implementation_function#
}
