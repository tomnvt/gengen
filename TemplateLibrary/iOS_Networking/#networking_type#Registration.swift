//
//  #networking_type#Registration.swift
//
//  Created by GenGen.
//

import Swinject

extension DataSourceDependencies {

    static func register#networking_type#(to container: Container) {
        container.register(#networking_type#.self) { r in
            #networking_type#Impl()
        }
    }
}
